package io.notify.me.di

import android.content.Context
import io.notify.me.database.NotifyMeDatabase
import org.koin.dsl.module

val dependency = module {
    single { provideDb(get()) }
}

fun provideDb(context: Context): NotifyMeDatabase {
    return NotifyMeDatabase.getInstance(context)
}