/*
 *
 *  * Created by farhan on 8/2/22, 9:54 AM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 8/2/22, 9:54 AM
 *
 */

package io.notify.me.enums

enum class NotifyMeEventType(val value: Int) {
    SMS_RECEIVED(0),
    CALL_RECEIVED(1),
    VOIP_CALL_RECEIVED(2);

    companion object {
        fun valueOf(value: Int) = NotifyMeEventType.values().find { it.value == value }
    }
}