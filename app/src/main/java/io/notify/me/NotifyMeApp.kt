package io.notify.me

import android.app.Application
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.util.Log
import androidx.annotation.RequiresApi
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import io.notify.me.di.dependency
import io.notify.me.event.CallEvent
import io.notify.me.event.SMSEvent
import io.notify.me.model.AppConfig
import io.notify.me.service.NotifyMeService
import io.notify.me.util.isServiceRunning
import org.greenrobot.eventbus.EventBus
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class NotifyMeApp : Application() {
    private val TAG = NotifyMeService::class.java.simpleName
    val APP_CONFIG_COLLECTION_NAME = "APP_CONFIG_COLLECTION"
    val APP_CONFIG_DOCUMENT_NAME = "APP_CONFIG"

    companion object {
        private var updateInterval = 15000L
        lateinit var instance: NotifyMeApp
            private set

        fun getNotifyMeUpdateInterval(): Long {
            return updateInterval
        }
    }

    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this) // Disable this while working with google-services enabled device
        startKoin {
            androidContext(applicationContext)
            modules(listOf(dependency))
        }
        instance = this
        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser == null) {
            FirebaseAuth.getInstance().signInAnonymously().addOnCompleteListener {
                if (it.isSuccessful) {
                    Log.d(TAG, "==== Login Success =======")
                    readAppConfigAndProceed()
                } else {
                    System.exit(100)
                }
            }
        } else {
            readAppConfigAndProceed()
        }
        Handler().postDelayed({
            EventBus.getDefault().post(SMSEvent("UNKNOWN111", "UNKOWN"))
            EventBus.getDefault().post(CallEvent("UNKNOWN"))
        }, 5000)
    }

    private fun startMySuperNotifyMeService() {
        Intent(instance, NotifyMeService::class.java).apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(this)
            }
        }
    }

    private fun readAppConfigAndProceed() {
        FirebaseFirestore.getInstance().collection(APP_CONFIG_COLLECTION_NAME)
            .document(APP_CONFIG_DOCUMENT_NAME)
            .get().addOnSuccessListener {
                if (it.exists()) {
                    try {
                        Log.d(TAG, "==== Received APP CONFIG =======")
                        val appConfig = it.toObject(AppConfig::class.java)
                        updateInterval = appConfig?.notifyIntervalInMilliSec ?: 15000L
                        startMySuperNotifyMeService()
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }
            }.addOnFailureListener { }
    }
}