/*
 *
 *  * Created by farhan on 4/5/22, 7:25 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 3/30/22, 11:44 AM
 *
 */

package io.notify.me.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.notify.me.database.entities.NotifyMeStore


@Dao
interface NotifyMeStoreDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(word: NotifyMeStore)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNotifyMeStoreList(word: List<NotifyMeStore>)

    @Query("DELETE FROM notify_me_store")
    suspend fun deleteAll()

    @Query("SELECT * FROM notify_me_store ORDER BY pid ASC")
    suspend fun getAllNotifyMeStore(): List<NotifyMeStore>?

    @Query("SELECT * FROM notify_me_store LIMIT 1")
    fun getNotifyMeStore(): NotifyMeStore?


    @Query("SELECT * FROM notify_me_store WHERE notified=0")
    fun getUnNotifiedList(): List<NotifyMeStore>?


    @Query("SELECT COUNT(*) FROM notify_me_store WHERE notified=0")
    fun getUnNotifiedLiveDataEvent(): LiveData<Int>?

}