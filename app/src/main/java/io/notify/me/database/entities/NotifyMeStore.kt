/*
 *
 *  * Created by farhan on 4/5/22, 7:25 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 4/5/22, 7:24 PM
 *
 */

package io.notify.me.database.entities

import android.os.Parcelable
import androidx.annotation.Keep
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize


@Entity(tableName = "notify_me_store")
@Keep
@Parcelize
data class NotifyMeStore(
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "pid")
    var pid: Long = 0,

    @ColumnInfo(name = "smsFrom")
    var smsFrom: String? = null,

    @ColumnInfo(name = "smsReceivedTime")
    var smsReceivedTime: Long? = null,

    @ColumnInfo(name = "smsContent")
    var smsContent: String? = null,


    @ColumnInfo(name = "type")
    var type: String? = null, // Either SMS or Call

    @ColumnInfo(name = "notified")
    var notified: Boolean? = false,


    @ColumnInfo(name = "callFrom")
    var callFrom: String? = null,

    @ColumnInfo(name = "callReceivedTime")
    var callReceivedTime: Long? = null,


    @ColumnInfo(name = "exception")
    var exception: String? = null,


    ) : Parcelable