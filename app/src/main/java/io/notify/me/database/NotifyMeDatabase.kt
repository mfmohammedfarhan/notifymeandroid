package io.notify.me.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import io.notify.me.database.dao.NotifyMeStoreDAO
import io.notify.me.database.entities.NotifyMeStore


@Database(
    entities = [
        NotifyMeStore::class
    ],
    version = 1, // update the verson of db everytime if there are changes in schema regardless of having migration steps or not.
    exportSchema = false
)
abstract class NotifyMeDatabase : RoomDatabase() {
    abstract fun notifyMeStoreDAO(): NotifyMeStoreDAO

    companion object {
        @Volatile
        private var instance: NotifyMeDatabase? = null
        fun getInstance(context: Context): NotifyMeDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): NotifyMeDatabase {
            return Room.databaseBuilder(context, NotifyMeDatabase::class.java, "notifyme.db")
                .addCallback(
                    object : RoomDatabase.Callback() {
                    }
                )
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build()
        }
    }

}