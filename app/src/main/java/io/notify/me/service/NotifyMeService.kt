package io.notify.me.service

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import android.util.Log
import android.widget.RemoteViews
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.viewModelScope
import com.google.firebase.FirebaseApp
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import io.notify.me.NotifyMeApp
import io.notify.me.R
import io.notify.me.database.NotifyMeDatabase
import io.notify.me.database.entities.NotifyMeStore
import io.notify.me.event.CallEvent
import io.notify.me.event.SMSEvent
import io.notify.me.model.AppConfig
import io.notify.me.model.ReceivedCallModel
import io.notify.me.model.ReceivedSmsModel
import io.notify.me.ui.MainActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.sql.Time
import java.util.*


class NotifyMeService : NotificationListenerService() {
    private val TAG = NotifyMeService::class.java.simpleName
    lateinit var mNotificationManager: NotificationManager
    lateinit var customNotificationBuilder: NotificationCompat.Builder
    private lateinit var remoteViews: RemoteViews
    private var updateInterval = NotifyMeApp.getNotifyMeUpdateInterval()
    private var notificationId = (System.currentTimeMillis() % 10000).toInt()
    private var timer: Timer? = null
    private var timerTask: TimerTask? = null
    val firebaseFireStoreDB = FirebaseFirestore.getInstance()
    val SMS_COLLECTION_NAME = "SMS_COLLECTION"
    val CALL_COLLECTION_NAME = "CALL_COLLECTION"
    val notifyMeDatabase = NotifyMeDatabase.getInstance(NotifyMeApp.instance)
    private var IS_NOTIFY_ME_TIMER_TASK_RUNNING: Boolean = false
    private val WHATSAPP_PACKAGE = "com.whatsapp"

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate() {
        super.onCreate()
        EventBus.getDefault().register(this);



        mNotificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        val channel = NotificationChannel(
            "NOTIFY_ME", "NOTIFY_ME_CHANNEL", NotificationManager.IMPORTANCE_HIGH
        )
        channel.setShowBadge(false)
        mNotificationManager.createNotificationChannel(channel)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        startForeground(1, prepareCustomNotification(this))
        return START_NOT_STICKY
    }

    private fun prepareCustomNotification(
        context: Context
    ): Notification? {
        try {
            val notificationIntent = Intent(this, MainActivity::class.java)
            val pendingIntent =
                PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_MUTABLE)
            customNotificationBuilder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationCompat.Builder(
                    context, "NOTIFY_ME"
                )
            } else {
                NotificationCompat.Builder(context)
            }
            customNotificationBuilder.setAutoCancel(false)
            customNotificationBuilder.setOngoing(true)
            customNotificationBuilder.setSilent(true)
            customNotificationBuilder.setSmallIcon(R.drawable.ic_launcher_foreground)
            customNotificationBuilder.setContentTitle("Notify me")
            customNotificationBuilder.setContentIntent(pendingIntent)
            customNotificationBuilder.setContentText("So ready to notify you ------")
            customNotificationBuilder.setStyle(NotificationCompat.BigTextStyle())
            customNotificationBuilder.setVibrate(null)
            return customNotificationBuilder.build()
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }
    }

    override fun onNotificationPosted(sbn: StatusBarNotification?) {
        super.onNotificationPosted(sbn)
        sbn?.let {
            when (it.packageName) {
                WHATSAPP_PACKAGE -> {
                    it.notification.extras?.let {
                        Log.d(TAG, "Data received $it")
                    }
                }
                else -> {}
            }
        }
    }

    private fun updateNotification(
    ) {
        try {
            customNotificationBuilder.setContentTitle("")
            customNotificationBuilder.setContentText("")
            customNotificationBuilder.setVibrate(null)
            customNotificationBuilder.setSilent(true)
            mNotificationManager.notify(notificationId, customNotificationBuilder.build())
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }


    @Subscribe(threadMode = ThreadMode.ASYNC)
    public fun logIncomingSms(smsAppEvent: SMSEvent) {
        val notifyMeStore = NotifyMeStore()
        try {
            notifyMeStore.smsFrom = smsAppEvent.smsByInfo
            notifyMeStore.smsContent = smsAppEvent.smsDetails
            notifyMeStore.smsReceivedTime = Date().time
            notifyMeStore.type = "SMS"
            val receivedSmsModel = ReceivedSmsModel()
            receivedSmsModel.smsByInfo = smsAppEvent.smsByInfo
            receivedSmsModel.smsDetails = smsAppEvent.smsDetails
            receivedSmsModel.timestamp = Timestamp.now()
            firebaseFireStoreDB.collection(SMS_COLLECTION_NAME).add(receivedSmsModel)
                .addOnSuccessListener {
                    notifyMeStore.notified = true
                    addOrUpdateNotifyStore(notifyMeStore)
                }.addOnFailureListener {
                    notifyMeStore.notified = false
                    addOrUpdateNotifyStore(notifyMeStore)
                }
        } catch (e: Exception) {
            e.printStackTrace()
            notifyMeStore.notified = false
            notifyMeStore.exception = e.message
            addOrUpdateNotifyStore(notifyMeStore)
        }
    }


    @Subscribe(threadMode = ThreadMode.ASYNC)
    public fun logIncomingCall(callAppEvent: CallEvent) {
        val notifyMeStore = NotifyMeStore()
        try {
            notifyMeStore.callFrom = callAppEvent.incomingPhoneNumber
            notifyMeStore.callReceivedTime = Date().time
            notifyMeStore.type = "CALL"
            val receivedCallModel = ReceivedCallModel()
            receivedCallModel.incomingPhoneNumber = callAppEvent.incomingPhoneNumber
            receivedCallModel.timestamp = Timestamp.now()
            firebaseFireStoreDB.collection(CALL_COLLECTION_NAME).add(receivedCallModel)
                .addOnSuccessListener {
                    notifyMeStore.notified = false
                    addOrUpdateNotifyStore(notifyMeStore)

                }.addOnFailureListener {
                    notifyMeStore.notified = false
                    addOrUpdateNotifyStore(notifyMeStore)
                }
        } catch (e: Exception) {
            e.printStackTrace()
            notifyMeStore.notified = false
            notifyMeStore.exception = e.message
            addOrUpdateNotifyStore(notifyMeStore)
        }
    }

    private fun addOrUpdateNotifyStore(notifyMeStore: NotifyMeStore) {
        synchronized(this) {
            notifyMeDatabase.notifyMeStoreDAO().insert(notifyMeStore)
            if (notifyMeStore.notified == false) {
                retryToNotify()
            }
        }
    }

    private fun retryToNotify() {
        synchronized(this) {
            stopTimerTask()
            timerTask = object : TimerTask() {
                override fun run() {
                    val unNotifiedList = notifyMeDatabase.notifyMeStoreDAO().getUnNotifiedList()
                    if (unNotifiedList.isNullOrEmpty()) {
                        stopTimerTask()
                        Log.d(
                            TAG,
                            "============ Stopping timer task as no more un-notified task are available ============"
                        )
                    } else {
                        Log.d(TAG, "============ Retrying un-notified task to notify ============")
                        unNotifiedList.forEach {
                            when (it.type) {
                                "CALL" -> {
                                    retryToNotifyReceivedCall(it)
                                }
                                "SMS" -> {
                                    retryToNotifyReceivedSMS(it)
                                }
                                else -> {
                                    Log.d(
                                        TAG,
                                        "============ Skipped for unknown type of un-notified task to notify ============"
                                    )
                                }
                            }
                        }
                    }
                }
            }
            timer = Timer()
            timer?.schedule(timerTask, 0, updateInterval)
            IS_NOTIFY_ME_TIMER_TASK_RUNNING = true
        }
    }

    private fun stopTimerTask() {
        timer?.cancel().also {
            timer?.purge()
            timer = null
        }
        timerTask?.cancel().also { timerTask = null }
        IS_NOTIFY_ME_TIMER_TASK_RUNNING = false
    }

    private fun retryToNotifyReceivedCall(notifyMeStore: NotifyMeStore) {
        try {
            val receivedCallModel = ReceivedCallModel()
            receivedCallModel.incomingPhoneNumber = notifyMeStore.callFrom
            receivedCallModel.timestamp =
                if (notifyMeStore.callReceivedTime == null) Timestamp.now() else Timestamp(
                    Date(notifyMeStore.callReceivedTime!!)
                )
            firebaseFireStoreDB.collection(CALL_COLLECTION_NAME).add(receivedCallModel)
                .addOnSuccessListener {
                    notifyMeStore.notified = true
                    addOrUpdateNotifyStore(notifyMeStore)
                }.addOnFailureListener {
                    notifyMeStore.notified = false
                    addOrUpdateNotifyStore(notifyMeStore)
                }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            notifyMeStore.notified = false
            notifyMeStore.exception = e.message
            addOrUpdateNotifyStore(notifyMeStore)
        }
    }

    private fun retryToNotifyReceivedSMS(notifyMeStore: NotifyMeStore) {
        try {
            val receivedSmsModel = ReceivedSmsModel()
            receivedSmsModel.smsByInfo = notifyMeStore.smsFrom
            receivedSmsModel.smsDetails = notifyMeStore.smsContent
            receivedSmsModel.timestamp =
                if (notifyMeStore.smsReceivedTime == null) Timestamp.now() else Timestamp(
                    Date(notifyMeStore.smsReceivedTime!!)
                )
            firebaseFireStoreDB.collection(SMS_COLLECTION_NAME).add(receivedSmsModel)
                .addOnSuccessListener {
                    notifyMeStore.notified = true
                    addOrUpdateNotifyStore(notifyMeStore)
                }.addOnFailureListener {
                    notifyMeStore.notified = false
                    addOrUpdateNotifyStore(notifyMeStore)
                }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }
}