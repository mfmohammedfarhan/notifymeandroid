package io.notify.me.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.telephony.SmsMessage
import io.notify.me.event.SMSEvent
import org.greenrobot.eventbus.EventBus

class SMSBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent?) {
        if (intent != null && intent.action != null && intent.action!!.equals(
                "android.provider.Telephony.SMS_RECEIVED",
                ignoreCase = true
            )
        ) {
            val bundle = intent.extras
            if (bundle != null) {
                val sms = bundle.get(SMS_BUNDLE) as Array<Any>?
                val smsMsg = StringBuilder()

                var smsMessage: SmsMessage
                if (sms != null) {
                    for (sm in sms) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            val format = bundle.getString("format")
                            smsMessage = SmsMessage.createFromPdu(sm as ByteArray, format)
                        } else {
                            smsMessage = SmsMessage.createFromPdu(sm as ByteArray)
                        }
                        val msgBody = smsMessage.messageBody.toString()
                        val msgAddress = smsMessage.originatingAddress
                        val smsEvent = SMSEvent(msgAddress ?: "UNKNOWN", msgBody)
                        EventBus.getDefault().post(smsEvent)
                    }
                }
            }
        }
    }

    companion object {
        val SMS_BUNDLE = "pdus"
    }
}