package io.notify.me.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.metrics.Event
import android.telephony.PhoneStateListener

import android.telephony.TelephonyManager
import io.notify.me.event.CallEvent
import org.greenrobot.eventbus.EventBus

class CallBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val telephony = context!!.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        telephony.listen(object : PhoneStateListener() {
            override fun onCallStateChanged(state: Int, incomingNumber: String) {
                super.onCallStateChanged(state, incomingNumber)
                println("incomingNumber : $incomingNumber")
                val callEvent = CallEvent(incomingNumber)
                EventBus.getDefault().post(callEvent)
            }
        }, PhoneStateListener.LISTEN_CALL_STATE)
    }
}