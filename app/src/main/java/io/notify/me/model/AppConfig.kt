package io.notify.me.model

import android.util.Log

data class AppConfig(
    var notifyEnabled: Boolean? = null,
    var notifyIntervalInMilliSec: Long? = null
)