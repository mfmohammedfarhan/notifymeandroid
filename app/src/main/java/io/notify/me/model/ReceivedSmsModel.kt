package io.notify.me.model

import com.google.firebase.Timestamp
import com.google.firebase.firestore.ServerTimestamp
import java.util.*

data class ReceivedSmsModel(
    var smsByInfo: String? = null,
    var smsDetails: String? = null,
    @ServerTimestamp var timestamp: Timestamp? = null
)