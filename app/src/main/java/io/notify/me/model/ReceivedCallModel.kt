package io.notify.me.model

import com.google.firebase.Timestamp
import com.google.firebase.firestore.ServerTimestamp
import java.util.*

data class ReceivedCallModel(
    var incomingPhoneNumber: String? = null,
    @ServerTimestamp var timestamp: Timestamp? = null
)