package io.notify.me.event

data class CallEvent(var incomingPhoneNumber: String)
data class SMSEvent(var smsByInfo: String, var smsDetails: String)